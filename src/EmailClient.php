<?php

namespace DexDgtl\EmailSDK;

use DexDgtl\EmailSDK\Drivers\DriverConfig;
use DexDgtl\EmailSDK\Exceptions\EmailDriverException;

/**
 * Class EmailClient
 * @package DexDgtl\EmailSDK
 */
class EmailClient
{
    private $driver;

    /**
     * EmailClient constructor.
     * @param DriverConfig $config
     * @throws EmailDriverException
     */
    public function __construct(DriverConfig $config)
    {
        $driverName = $config->getDriver();

        if (!$this->hasDriver($driverName)) {
            throw new EmailDriverException('Unsupported driver!');
        }

        $this->driver = new $driverName($config);
    }

    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param string $driverName
     * @return bool
     */
    public function hasDriver($driverName)
    {
        return class_exists($driverName);
    }
}