<?php

namespace DexDgtl\EmailSDK\Drivers;

use DexDgtl\EmailSDK\IMessage;

/**
 * Interface IDriver
 * @package DexDgtl\EmailSDK\Drivers
 */
interface IDriver
{
    public function createMessage();

    /**
     * @param IMessage $message
     * @return mixed
     */
    public function send(IMessage $message);
}