<?php

namespace DexDgtl\EmailSDK\Drivers\Mandrill;

/**
 * Class Template
 * @package DexDgtl\EmailSDK\Drivers\Mandrill
 */
class Template
{
    public $name;
    public $content = [];

    public function addContent(array $content)
    {
        $this->content[] = $content;
    }
}