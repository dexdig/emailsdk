<?php

namespace DexDgtl\EmailSDK\Drivers\Mandrill;

/**
 * Class Recipient
 * @package DexDgtl\EmailSDK\Drivers\Mandrill
 */
class Recipient
{
    public $email;
    public $name;
    public $type;
}