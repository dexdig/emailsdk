<?php

namespace DexDgtl\EmailSDK\Drivers\Mandrill;

use DexDgtl\EmailSDK\Drivers\DriverConfig;
use DexDgtl\EmailSDK\Drivers\IDriver;
use DexDgtl\EmailSDK\IMessage;
use Mandrill;

/**
 * Class MandrillDriver
 * @package DexDgtl\EmailSDK\Drivers\Mandrill
 */
class MandrillDriver implements IDriver
{
    protected $apiKey;
    protected $messages = [];
    protected $tags = [];
    protected $templates = [];
    protected $senders = [];

    private $config;
    private $client;

    public function __construct(DriverConfig $config)
    {
        $this->config = $config;
        $this->client = new Mandrill($this->config->getSecret());
    }

    /**
     * @return MandrillMessage
     */
    public function createMessage()
    {
        return new MandrillMessage();
    }

    /**
     * @return Recipient
     */
    public function createRecipient()
    {
        return new Recipient();
    }

    /**
     * @return Template
     */
    public function createTemplate()
    {
        return new Template();
    }

    /**
     * @return Attachment
     */
    public function createAttachment()
    {
        return new Attachment();
    }

    /**
     * @param string $template
     * @param string $content
     * @param IMessage $message
     * @param bool $async
     * @param null $ipPool
     * @param string|null $sendAt
     * @return mixed
     */
    public function sendTemplate(
        Template $template,
        IMessage $message,
        $async = false,
        $ipPool = null,
        $sendAt = null
    )
    {
        if (!sizeof($template->content)) {
            $template->content = [''];
        }

        return $this->client->messages->sendTemplate($template->name, $template->content, get_object_vars($message), $async, $ipPool, $sendAt);
    }

    public function send(IMessage $message, $async = false, $ipPool = null, $sendAt = null)
    {
        return $this->client->messages->send(get_object_vars($message), $async, $ipPool, $sendAt);
    }
}