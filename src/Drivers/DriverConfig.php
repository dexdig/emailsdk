<?php

namespace DexDgtl\EmailSDK\Drivers;

use DexDgtl\EmailSDK\Exceptions\EmailDriverException;

/**
 * Class DriverConfig
 * @package DexDgtl\EmailSDK\Drivers
 */
class DriverConfig
{
    private $data;

    /**
     * DriverConfig constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     * @throws EmailDriverException
     */
    public function getSecret()
    {
        return $this->getRequiredProperty('secret');
    }

    /**
     * Get class name driver
     * @return string
     * @throws EmailDriverException
     */
    public function getDriver()
    {
        return $this->getRequiredProperty('driver');
    }

    /**
     * @param string $ident
     * @return mixed
     * @throws EmailDriverException
     */
    public function getRequiredProperty($ident)
    {
        if (!array_key_exists($ident, $this->data)) {
            throw new EmailDriverException("$ident require property!");
        }

        return $this->data[$ident];
    }
}