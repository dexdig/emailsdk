### Install

composer.json
```json
{
    "require": {
      "dex/emailsdk": "dev-master"
    },
    "repositories": [
          {
            "type": "vcs",
            "url": "https://bitbucket.org/dexdig/emailsdk.git"
          }
    ]
}
```
Run command
`composer update`

### For Laravel

Run command
`php artisan vendor:publish --provider="DexDgtl\EmailSDK\EmailSDKServiceProvider"`

### Send email by mandrill driver
```php

$emailConfig = new DriverConfig([
    'secret' => 'secret_key',
    'driver' => MandrillDriver::class
]);

$emailClient = new EmailClient($emailConfig);

$message = $emailClient->getDriver()->createMessage();

$message->text       = 'Hello, Test';
$message->html       = '<h1>Test</h1>';
$message->subject    = 'Test';
$message->from_email = 'info@client-space.net';
$message->from_name  = 'Mandrill Test';
$message->subaccount = 'sys_clientspace';

$recipient = $emailClient->getDriver()->createRecipient();
$recipient->email = 'vip93951247@gmail.com';
$recipient->name  = 'Recipient Name';
$recipient->type  = 'to';

$message->addRecipient($recipient);

$emailClient->getDriver()->send($message);

// OR sendTemplate

$template = $emailClient->getDriver()->createTemplate();
$template->name = 'test';
$template->addContent([
    "name"    => 'body_mess',
    "content" => 'test'
]);

$emailClient->getDriver()->sendTemplate($template, $message);

```